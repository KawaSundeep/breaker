package com.game.breaker;

public class Constants {

  static final int FRAME_WIDTH = 1000;
  static final int FRAME_HEIGHT = 700;

  static final int BRICK_WIDTH = 50;
  static final int BRICK_HEIGHT = 25;

  static final int BALL_WIDTH = 20;
  static final int BALL_HEIGHT = 20;
  static final int BALL_DIAMETER = 20;

  static long BALL_SPEED = 10;
  static int BALL_DX = 2;
  static int BALL_DY = 3;

  static boolean isWait = true;

  static int BAR_WIDTH = 100;
  static final int BAR_HEIGHT = 5;

  static int BAR_MOVEMENT = 25;

  static final String SCORE = "score: ";
  static final String START = "key : s-start, left/right- move bar to left/right, (hold left/right to move fast)";
}
