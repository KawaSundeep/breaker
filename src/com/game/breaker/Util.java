package com.game.breaker;

public class Util {

	public static boolean isBottom(Ball ball, Brick brick) {
		if (ball.getLeft() > brick.getLeft()
				&& ball.getLeft() < brick.getRight()) {
			if (ball.getTop() < brick.getBottom()
					&& ball.getTop() > brick.getTop()) {
				return true;
			}
		}
		return false;
	}

	public static boolean isTop(Ball ball, Brick brick) {
		if (ball.getLeft() > brick.getLeft()
				&& ball.getLeft() < brick.getRight()) {
			if (ball.getBottom() > brick.getTop()
					&& ball.getBottom() < brick.getBottom()) {
				return true;
			}
		}
		return false;
	}

	public static boolean isRight(Ball ball, Brick brick) {
		if (ball.getLeft() > brick.getLeft()
				&& ball.getLeft() < brick.getRight()) {
			if (ball.getTop() > brick.getTop()
					&& ball.getTop() < brick.getBottom()) {
				return true;
			}
		}
		return false;
	}

	public static boolean isLeft(Ball ball, Brick brick) {
		if (ball.getRight() > brick.getLeft()
				&& ball.getRight() < brick.getRight()) {
			if (ball.getTop() > brick.getTop()
					&& ball.getTop() < brick.getBottom()) {
				return true;
			}
		}
		return false;
	}

	public static boolean isLandedOnBar(Bar bar, Ball ball) {
		if (ball.getRight() > bar.getX()
				&& ball.getLeft() < (bar.getX() + Constants.BAR_WIDTH)) {
			return true;
		}
		return false;
	}
}
