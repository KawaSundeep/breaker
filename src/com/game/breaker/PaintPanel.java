package com.game.breaker;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;

class PaintPanel extends JPanel implements KeyListener {
  private static final long serialVersionUID = 1L;
  Ball ball;
  List<Brick> bricks;
  Bar bar;
  int score;
  private int life = 3;
  private boolean isFirst = true;
  private int power = 1;

  PaintPanel() {
    feedValues();
    score = 0;
    this.setFocusable(true);
    this.addKeyListener(this);
    this.setBackground(new Color(240, 230, 140));
  }

  protected void paintComponent(Graphics g) {
    super.paintComponent(g);

    drawBall(g);
    drawBricks(g);
    drawBar(g);
    drawScore(g);

  }

  private void drawScore(Graphics g) {
    g.drawString(Constants.START, 10, Constants.FRAME_HEIGHT - 35);
    g.setColor(Color.RED);
    g.drawString(Constants.SCORE + "" + score, 10,
                 Constants.FRAME_HEIGHT - 50);

    g.drawString("Life: " + life, 10, 20);
  }
  private void drawBar(Graphics g) {
    g.drawRoundRect(bar.getX(), bar.getY(), Constants.BAR_WIDTH,
                    Constants.BAR_HEIGHT, 10, 5);
    g.setColor(Color.BLACK);
    g.fillRoundRect(bar.getX(), bar.getY(), Constants.BAR_WIDTH,
                    Constants.BAR_HEIGHT, 10, 5);
  }

  private void drawBall(Graphics g) {
    moveBall();
    g.setColor(Color.DARK_GRAY);
    g.fillOval(ball.getLeft(), ball.getTop(), Constants.BALL_WIDTH,
               Constants.BALL_HEIGHT); // adds color to circle
    g.setColor(Color.black);
    g.drawOval(ball.getLeft(), ball.getTop(), Constants.BALL_WIDTH,
               Constants.BALL_HEIGHT); // draws circle

  }

  private void drawBricks(Graphics g) {
    for (Brick brick : bricks) {
      if (!brick.isBroken) {
        if (brick.isSpecial) {
          g.setColor(new Color(255, 215, 0));
          g.fillRoundRect(brick.getLeft(), brick.getTop(),
                          Constants.BRICK_WIDTH, Constants.BRICK_HEIGHT, 10, 10);
        } else {
          g.setColor(new Color(99, 33, 00));
          g.fillRoundRect(brick.getLeft(), brick.getTop(),
                          Constants.BRICK_WIDTH, Constants.BRICK_HEIGHT, 10, 10);
        }
        g.setColor(Color.BLACK);
        g.drawRoundRect(brick.getLeft(), brick.getTop(),
                        Constants.BRICK_WIDTH, Constants.BRICK_HEIGHT, 10, 10);

      }
    }

    if (bricks.size() == 0) {
      System.out.println("Game Cleared");
      Constants.isWait = true;
    }
  }

  public void moveBall() {
    boolean isDirChanged = false;
    if (ball.getLeft() + Constants.BALL_DX < 0
        || ball.getRight() + Constants.BALL_DX > getWidth()) {
      Constants.BALL_DX *= -1;
      isDirChanged = true;
    }

    if (ball.getBottom() + Constants.BALL_DY > bar.getY()) {
      if (Util.isLandedOnBar(bar, ball)) {
        Constants.BALL_DY *= -1;
        isDirChanged = true;
      } else {
        life--;
        feedValues();
        Constants.isWait = true;
      }
    }

    if (ball.getTop() + Constants.BALL_DY < 0) {
      Constants.BALL_DY *= -1;
      isDirChanged = true;
    }

    if (!isDirChanged)
      for (int i = 0; i < bricks.size(); i++) {
        Brick brick = bricks.get(i);
        if (Util.isBottom(ball, brick) || Util.isTop(ball, brick)) {
          if (brick.isSpecial) {
            switch (power) {
              case 1 :
                extendBar();
                power++;
                break;
              case 2 :
                life++;
                power++;
                break;
              case 3 :
                extendBar();
                increaseSpeed();
                power++;
                break;
            }

          }
          Constants.BALL_DY *= -1;
          bricks.remove(i);
          score += 2;
          break;
        } else if (Util.isLeft(ball, brick)
                   || Util.isRight(ball, brick)) {
          Constants.BALL_DX *= -1;
          bricks.remove(i);
          score += 2;
          break;
        }
      }
    ball.setLeft(ball.getLeft() + Constants.BALL_DX);
    ball.setTop(ball.getTop() + Constants.BALL_DY);
  }
  private void increaseSpeed() {
    Constants.BALL_SPEED = 8;
    new Thread(new Runnable() {

      @Override
      public void run() {
        try {
          Thread.sleep(10000);
          Constants.BALL_SPEED = 10;
          System.out.println("increased speed");
        } catch (InterruptedException e) {
          System.out.println("Exception while increasing speed: " + e.getMessage());
        }
      }

    }).start();
  }

  private void extendBar() {
    Constants.BAR_WIDTH = 200;
    new Thread(new Runnable() {

      @Override
      public void run() {
        try {
          Thread.sleep(10000);
          Constants.BAR_WIDTH = 100;
          System.out.println("increased bar width");
        } catch (InterruptedException e) {
          System.out.println("Exception while increasing bar width: " + e.getMessage());
        }
      }

    }).start();
  }

  @Override
  public void keyPressed(KeyEvent e) {
    switch (e.getKeyCode()) {
      case KeyEvent.VK_LEFT :
        if (bar.getX() > 0) {
          int distanceLeft = getWidth() - bar.getX();
          if (distanceLeft > Constants.BAR_MOVEMENT) {
            bar.setX(bar.getX() - Constants.BAR_MOVEMENT);
          } else {
            bar.setX(bar.getX() - distanceLeft);
          }
        }
        break;
      case KeyEvent.VK_RIGHT :
        if (bar.getX() + Constants.BAR_WIDTH < getWidth()) {
          int distanceLeft = getWidth() - bar.getX()
                             + Constants.BAR_WIDTH;
          if (distanceLeft > Constants.BAR_MOVEMENT) {
            bar.setX(bar.getX() + Constants.BAR_MOVEMENT);
          } else {
            bar.setX(bar.getX() + distanceLeft);
          }
        }
        break;

      case KeyEvent.VK_S :
        Constants.isWait = false;
        break;
    }
  }

  @Override
  public void keyReleased(KeyEvent e) {
    // TODO Auto-generated method stub

  }

  @Override
  public void keyTyped(KeyEvent e) {
    // TODO Auto-generated method stub

  }

  private void feedValues() {
    if (life == 0 || isFirst) {
      bricks = new ArrayList<Brick>();
      for (int i = 2 * Constants.BRICK_WIDTH; i < Constants.FRAME_WIDTH
                                                  - (2 * Constants.BRICK_WIDTH); i += Constants.BRICK_WIDTH) {
        for (int j = 3 * Constants.BRICK_HEIGHT; j < 300; j += Constants.BRICK_HEIGHT) {
          bricks.add(new Brick(i, j, false));
        }
      }
      life = 3;
      isFirst = false;

      // make some bars special
      Random r = new Random();
      for (int i = 0; i < 3; i++) {
        bricks.get(r.nextInt(bricks.size())).setSpecial(true);
      }
      score = 0;
    }
    bar = new Bar((Constants.FRAME_WIDTH - Constants.BAR_WIDTH) / 2,
                  Constants.FRAME_HEIGHT - 100);
    ball = new Ball(Constants.FRAME_WIDTH / 2, bar.getY() - Constants.BALL_DIAMETER);

  }

}
