package com.game.breaker;

public class Ball {
	protected int x, y;

	public Ball(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getLeft() {
		return x;
	}

	public int getTop() {
		return y;
	}

	public int getRight() {
		return x + Constants.BALL_WIDTH;
	}

	public int getBottom() {
		return y + Constants.BALL_HEIGHT;
	}

	public void setLeft(int x) {
		this.x = x;
	}

	public void setTop(int y) {
		this.y = y;
	}

}
