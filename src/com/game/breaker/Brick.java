package com.game.breaker;

public class Brick {
  protected int x, y;
  protected boolean isBroken;
  protected boolean isSpecial;

  public Brick(int x, int y, boolean isBroken) {
    this.x = x;
    this.y = y;
    this.isBroken = isBroken;
  }

  public int getLeft() {
    return x;
  }

  public int getRight() {
    return x + Constants.BRICK_WIDTH;
  }

  public int getTop() {
    return y;
  }

  public int getBottom() {
    return y + Constants.BRICK_HEIGHT;
  }

  /*
   * public void setX(int x) { this.x = x; }
   */

  /*
   * public void setY(int y) { this.y = y; }
   */

  public boolean isBroken() {
    return isBroken;
  }

  public void setBroken(boolean isBroken) {
    this.isBroken = isBroken;
  }

  public boolean isSpecial() {
    return isSpecial;
  }

  public void setSpecial(boolean isSpecial) {
    this.isSpecial = isSpecial;
  }

}
