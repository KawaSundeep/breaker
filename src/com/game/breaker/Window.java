package com.game.breaker;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Window {

	Thread thread;
	Runnable runnable;
	PaintPanel panel;

	public Window() {
		JFrame jFrm = new JFrame("Breaker");
		jFrm.setSize(Constants.FRAME_WIDTH + 10, Constants.FRAME_HEIGHT);
		jFrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel = new PaintPanel();
		jFrm.add(panel);
		jFrm.setResizable(false);
		jFrm.setVisible(true);

		runnable = new Runnable() {

			@Override
			public void run() {
				while (true) {
					if (!Constants.isWait) {
						panel.repaint();
					}
					try {
						Thread.sleep(Constants.BALL_SPEED);
					} catch (InterruptedException e) {
						System.out.println(e.getMessage());
					}
				}
			}
		};
		thread = new Thread(runnable);
		thread.setPriority(Thread.NORM_PRIORITY);
		thread.start();
	}

	public static void main(String d[]) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new Window();
			}
		});
	}
}